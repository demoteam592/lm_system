package com.elibrary.service;

import java.util.List;



public interface Crudservice<T> {
	public void add(T t);

	public void update(T t);

	public List<T> list();

	public T getById(int t);

	public void remove(int t);

}
