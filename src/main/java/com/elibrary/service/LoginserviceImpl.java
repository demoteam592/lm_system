package com.elibrary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elibrary.dao.UserDao;
import com.elibrary.entity.User;

@Service
public class LoginserviceImpl  {

	@Autowired
	private UserDao userDao;

	public void login(User user) {
		this.userDao.finduser(user);
	}
	public void passwordchange(int userid,String password){
		this.userDao.changepassword(userid, password);
	}
	
	
}