package com.elibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elibrary.dao.UserDao;
import com.elibrary.entity.User;

@Service
public class UserserviceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public void add(User user) {
		this.userDao.saveOrupdate(user);
	}

	@Override
	public void update(User user) {
		this.userDao.edit(user);
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public List<User> list() {
		// TODO Auto-generated method stub

		return this.userDao.find();
	}

	@Override
	public User getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(int id) {
		// TODO Auto-generated method stub
		this.userDao.delete(id);
	}
	
	public void update1(int userid,User user) {
		this.userDao.edit1(userid,user);
		// TODO Auto-generated method stub
		return;
	}
	

}
