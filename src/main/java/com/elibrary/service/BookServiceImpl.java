package com.elibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elibrary.dao.BookDao;
import com.elibrary.entity.Book;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDao bookDao;

	@Override
	public void add(Book b) {
		this.bookDao.saveOrupdate(b);
	}

	@Override
	public void update(Book book) {
		this.bookDao.edit(book);

	}

	@Override
	public List<Book> list() {
		return this.bookDao.find();

	}

	@Override
	public Book getById(int b) {
		return null;
	}

	@Override
	public void remove(int b) {
		this.bookDao.delete(b);
	}

}
