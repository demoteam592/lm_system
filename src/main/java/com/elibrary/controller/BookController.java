package com.elibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elibrary.entity.Book;
import com.elibrary.service.BookServiceImpl;


@Controller
@RequestMapping("/books")
public class BookController {
	@Autowired
	// @Qualifier("bookservice")
	private BookServiceImpl bookservice;

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public void addbook(@RequestBody Book book) {
		// Crudservice bookservice = new BookService();
		bookservice.add(book);
	}

	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public @ResponseBody List<Book> getbooklist() {
		List<Book> book = bookservice.list();
		return book;
	}
	@RequestMapping(method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json",value="/delete/{id}")
	public void deletebook(@PathVariable("id")int id){
		bookservice.remove(id);
		
	}
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json",value="/edit")
	public void editBook(Book book){
		bookservice.update(book);
		
	}

}
