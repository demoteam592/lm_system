package com.elibrary.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.elibrary.entity.User;
import com.elibrary.service.LoginserviceImpl;
@Controller
//@Secured("ROLE_ADMIN")
@RequestMapping("/login")
public class LoginController {
	@Autowired
	LoginserviceImpl loginservice;
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public void login(@RequestBody User user){
		loginservice.login(user);
	}
	
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json",value="/{id}")
	public void passwordRecovery(@PathVariable("id") int userid,  @RequestBody String password){
		loginservice.passwordchange(userid, password);
	}
	

}
