package com.elibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elibrary.entity.User;

import com.elibrary.service.UserserviceImpl;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserserviceImpl userservice;

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public void adduser(@RequestBody User user) {
		userservice.add(user);
		return;
	}

	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	// @Respo
	public @ResponseBody List<User> getuserlist() {

		List<User> user = userservice.list();
		return user;
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json", value = "/{id}")
	public void updateuser(@PathVariable("id") int userid, @RequestBody User user) {
		userservice.update1(userid,user);
		return;
	}

	@RequestMapping(method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json", value = "/delete/{id}")
	public void deleteuser(@PathVariable("id") int id) {
		userservice.remove(id);
	}

}
