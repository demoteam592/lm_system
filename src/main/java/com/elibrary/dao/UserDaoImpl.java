package com.elibrary.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.access.prepost.PreAuthorize;
//import org.hibernate.Transaction;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elibrary.entity.User;
import com.elibrary.entity.UserType;

@Repository
public class UserDaoImpl implements UserDao {

	SessionFactory sessionfactory;

	public void setSessionfactory(SessionFactory sessionfactory) {
		this.sessionfactory = sessionfactory;
	}

	@Override
	@Transactional
	public void saveOrupdate(User user) {
		Session session = this.sessionfactory.getCurrentSession();
		// Transaction trans=session.beginTransaction();
		session.persist(user);

		// / trans.commit();

	}

	@Override
	@Transactional
	public List<User> find() {
		Session session = this.sessionfactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<User> userlist = session.createQuery("from User").list();
		// System.out.println(userlist);
		// TODO Auto-generated method stub
		return userlist;
	}



	@Override
	@Transactional
	public void delete(int id) {
		Session session = this.sessionfactory.getCurrentSession();
		// TODO Auto-generated method stub
		User dltuser = (User) session.load(User.class, new Integer(id));
		session.delete(dltuser);

	}
@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Transactional
	public void finduser(User user) {
		Session session = this.sessionfactory.getCurrentSession();
		session.createQuery("select * from User  where email=:email AND password=:password")
				.setParameter("email", ":email")
				.setParameter("password", ":password");

	}
@Override
@Transactional
public void edit(User user) {
	  
	   
	  // Retrieve session from Hibernate
	  Session session = sessionfactory.getCurrentSession();
	   
	  // Retrieve existing person via id
	  User existingUser = (User) session.get(User.class, user.getUserid());
	   
	  // Assign updated values to this person
	  existingUser.setFirstname(user.getFirstname());
	  existingUser.setLastname(user.getLastname());
	  //existingUser.setMoney(user.getMoney());
	 
	  // Save updates
	  session.save(existingUser);
	 }
@Transactional
  public void  changepassword(int userid,String password){
	Session session = sessionfactory.getCurrentSession();
User user=(User)session.load(User.class, new Integer(userid));
user.setPassword(password);
user.setUsertype(UserType.admin);

session.saveOrUpdate(user);
}
@Transactional
public void edit1(int userid,User user){
	
	 Session session = sessionfactory.getCurrentSession();
	   
	  // Retrieve existing person via id
	  User existingUser = (User) session.get(User.class, user.getUserid());
	   
	  // Assign updated values to this person
	  existingUser.setFirstname(user.getFirstname());
	  existingUser.setLastname(user.getLastname());
	  existingUser.setPassword(user.getPassword());
	  existingUser.setEmail(user.getEmail());
	  existingUser.setPhonenumber(user.getPhonenumber());
	  existingUser.setUsertype(user.getUsertype());
	  
	  //existingUser.setMoney(user.getMoney());
	 
	  // Save updates
	  session.save(existingUser);
	 }
}


