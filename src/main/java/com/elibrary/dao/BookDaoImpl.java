package com.elibrary.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elibrary.entity.Book;


@Repository
public class BookDaoImpl implements BookDao {

	private SessionFactory sessionfactory;

	public void setSesssionfactory(SessionFactory sesssionfactory) {
		this.sessionfactory = sesssionfactory;
	}

	@Override
	@Transactional
	public void saveOrupdate(Book book) {
		Session session = this.sessionfactory.getCurrentSession();
		// Transaction trans=session.beginTransaction();
		session.persist(book);

		// System.out.println(userlist);

	}

	@Override
	@Transactional
	public List<Book> find() {
		Session session = this.sessionfactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Book> booklist = session.createQuery("from Book").list();
		return booklist;
	}

	


	@Override
	@Transactional
	public void delete(int isbn) {
		// TODO Auto-generated method stub
		Session session = this.sessionfactory.getCurrentSession();
		// TODO Auto-generated method stub
		Book dltbook = (Book) session.load(Book.class, new Integer(isbn));
		session.delete(dltbook);
	}
	@Override
	@Transactional
	public void edit(Book book){
		  // Retrieve session from Hibernate
		  Session session = sessionfactory.getCurrentSession();
		   
		  // Retrieve existing person via id
		  Book existingBook = (Book) session.get(Book.class, book.getIsbn());
		   
		  // Assign updated values to this person
		  existingBook.setTitle(book.getTitle());
		  existingBook.setAuthor(book.getAuthor());
		  //existingUser.setMoney(user.getMoney());
		 
		  // Save updates
		  session.save(existingBook);
		
	}

}
