package com.elibrary.dao;

import com.elibrary.entity.Book;

public interface BookDao extends Crud<Book> {

}
