package com.elibrary.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;







import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="user")
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3891834276378620755L;
	@Id
	@GenericGenerator(name = "userid", strategy = "increment")
	@GeneratedValue(generator = "userid")
	//@Access(value=AccessType.FIELD)
	private int userid;
	//@OneToOne
	@Column(name="firstname",nullable=false)
	private String firstname;
	//@OneToOne
	@Column(name="lastname",nullable=false)
	private String lastname;
	//@OneToOne
	@Column(name="password",nullable=false)
	private String password;
	//@OneToOne
	@Column(name="email",nullable=false)
	private String email;
	
	//@OneToOne
	@Column(name="phonenumber",nullable=false)
	private String phonenumber;
	//@OneToOne
	@Column(name="usertype",nullable=false,columnDefinition = "VARCHAR(10) NOT NULL")
	@Enumerated(EnumType.STRING)
	private UserType usertype;
	@OneToMany(mappedBy="user")
	
	
	private List<Book> book;
	
	
	

	public List<Book> getBook() {
		return book;
	}
	public void setBook(List<Book> book) {
		this.book = book;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public UserType getUsertype() {
		return usertype;
	}
	public void setUsertype(UserType usertype) {
		this.usertype = usertype;
	}
	

}
