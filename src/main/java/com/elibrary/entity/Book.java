package com.elibrary.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="book") 
public class Book implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1686962666463620847L;
	@Id
	@GenericGenerator(name = "isbn", strategy = "increment")
	@GeneratedValue(generator = "isbn")
	private int isbn;
	@Column(name="title",nullable=false)
	private String title;
	@Column(name="author",nullable=false)
	private String author;
	@Column(name="noofbooks",nullable=false)
	private int noofbooks;
	@Column(name="description",nullable=false)
	private String description;
	@ManyToOne
	@JoinColumn(name="book")
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getIsbn() {
		return isbn;
	}
	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getNoofbooks() {
		return noofbooks;
	}
	public void setNoofbooks(int noofbooks) {
		this.noofbooks = noofbooks;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
